class ItemsListData {
  ItemsListData({
    this.imagePath = '',
    this.titleTxt = '',
    this.startColor = '',
    this.endColor = '',
    this.kacl = 0,
    this.price = 0,
  });

  String imagePath;
  String titleTxt;
  String startColor;
  String endColor;
  int kacl;
  int price;

  static List<ItemsListData> tabIconsList = <ItemsListData>[
    ItemsListData(
      imagePath: 'assets/dashboard/breakfast.png',
      titleTxt: 'Breakfast',
      kacl: 580,
      price: 100,
      startColor: '#FA7D82',
      endColor: '#FFB295',
    ),
    ItemsListData(
      imagePath: 'assets/dashboard/lunch.png',
      titleTxt: 'Lunch',
      kacl: 630,
      price: 100,
      startColor: '#738AE6',
      endColor: '#5C5EDD',
    ),
    ItemsListData(
      imagePath: 'assets/dashboard/snack.png',
      titleTxt: 'Snack',
      kacl: 342,
      price: 100,
      startColor: '#FE95B6',
      endColor: '#FF5287',
    ),
    ItemsListData(
      imagePath: 'assets/dashboard/dinner.png',
      titleTxt: 'Dinner',
      kacl: 600,
      price: 100,
      startColor: '#6F72CA',
      endColor: '#1E1466',
    ),
    ItemsListData(
      imagePath: 'assets/dashboard/breakfast.png',
      titleTxt: 'Breakfast',
      kacl: 580,
      price: 100,
      startColor: '#FA7D82',
      endColor: '#FFB295',
    ),
    ItemsListData(
      imagePath: 'assets/dashboard/lunch.png',
      titleTxt: 'Lunch',
      kacl: 630,
      price: 100,
      startColor: '#738AE6',
      endColor: '#5C5EDD',
    ),
    ItemsListData(
      imagePath: 'assets/dashboard/snack.png',
      titleTxt: 'Snack',
      kacl: 342,
      price: 100,
      startColor: '#FE95B6',
      endColor: '#FF5287',
    ),
    ItemsListData(
      imagePath: 'assets/dashboard/dinner.png',
      titleTxt: 'Dinner',
      kacl: 600,
      price: 100,
      startColor: '#6F72CA',
      endColor: '#1E1466',
    ),
    ItemsListData(
      imagePath: 'assets/dashboard/breakfast.png',
      titleTxt: 'Breakfast',
      kacl: 580,
      price: 100,
      startColor: '#FA7D82',
      endColor: '#FFB295',
    ),
    ItemsListData(
      imagePath: 'assets/dashboard/lunch.png',
      titleTxt: 'Lunch',
      kacl: 630,
      price: 100,
      startColor: '#738AE6',
      endColor: '#5C5EDD',
    ),
    ItemsListData(
      imagePath: 'assets/dashboard/snack.png',
      titleTxt: 'Snack',
      kacl: 342,
      price: 100,
      startColor: '#FE95B6',
      endColor: '#FF5287',
    ),
    ItemsListData(
      imagePath: 'assets/dashboard/dinner.png',
      titleTxt: 'Dinner',
      kacl: 600,
      price: 100,
      startColor: '#6F72CA',
      endColor: '#1E1466',
    ),
  ];
}
