import 'package:flutter/material.dart';
import '../dashboard_theme.dart';
import '../models/items_list_data.dart';
import '../../../main.dart';

class ItemsListView extends StatefulWidget {
  const ItemsListView(
      {Key key, this.mainScreenAnimationController, this.mainScreenAnimation})
      : super(key: key);

  final AnimationController mainScreenAnimationController;
  final Animation<dynamic> mainScreenAnimation;

  @override
  _ItemsListViewState createState() => _ItemsListViewState();
}

class _ItemsListViewState extends State<ItemsListView>
    with TickerProviderStateMixin {
  AnimationController animationController;
  List<ItemsListData> mealsListData = ItemsListData.tabIconsList;

  @override
  void initState() {
    animationController = AnimationController(
        duration: const Duration(milliseconds: 2000), vsync: this);
    super.initState();
  }

  Future<bool> getData() async {
    await Future<dynamic>.delayed(const Duration(milliseconds: 50));
    return true;
  }

  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    return AnimatedBuilder(
      animation: widget.mainScreenAnimationController,
      builder: (BuildContext context, Widget child) {
        return FadeTransition(
          opacity: widget.mainScreenAnimation,
          child: Transform(
            transform: Matrix4.translationValues(
                0.0, 30 * (1.0 - widget.mainScreenAnimation.value), 0.0),
            child: Container(
              height: height,
              width: double.infinity,
              child: GridView.builder(
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 2,
                ),
                padding: EdgeInsets.only(
                  top: 24,
                  bottom: 180,
                  right: 16,
                  left: 16,
                ),
                itemCount: mealsListData.length,
                scrollDirection: Axis.vertical,
                physics: BouncingScrollPhysics(),
                itemBuilder: (BuildContext context, int index) {
                  final int count = mealsListData.length;
                  final Animation<double> animation =
                      Tween<double>(begin: 0.0, end: 1.0).animate(
                          CurvedAnimation(
                              parent: animationController,
                              curve: Interval((1 / count) * index, 1.0,
                                  curve: Curves.fastOutSlowIn)));
                  animationController.forward();

                  return ItemsView(
                    mealsListData: mealsListData[index],
                    animation: animation,
                    animationController: animationController,
                    indx: index,
                  );
                },
              ),
            ),
          ),
        );
      },
    );
  }
}

class ItemsView extends StatefulWidget {
  const ItemsView(
      {Key key,
      this.mealsListData,
      this.animationController,
      this.animation,
      this.indx})
      : super(key: key);

  final ItemsListData mealsListData;
  final AnimationController animationController;
  final Animation<dynamic> animation;
  final int indx;

  @override
  _ItemsViewState createState() => _ItemsViewState();
}

class _ItemsViewState extends State<ItemsView> {
  int pkts = 1;
  bool showOne = true;
  bool showTwo = false;
  bool showThree = false;
  bool showFour = false;
  bool showSummary = false;
  bool isSelected = false;
  List<RadioModel> sampleData = new List<RadioModel>();
  List<RadioModelDays> sampleDataDays = new List<RadioModelDays>();
  List<RadioModelDaysCustom> sampleDataDaysCustom =
      new List<RadioModelDaysCustom>();
  int count = 30;
  String days = 'Daily';
  List<String> customDays = ['M', 'TU', 'W', 'TH', 'F', 'SA', 'SU'];

  @override
  void initState() {
    super.initState();
    sampleData.add(new RadioModel(false, '3 Deliveries', 3));
    sampleData.add(new RadioModel(false, '6 Deliveries', 6));
    sampleData.add(new RadioModel(false, '15 Deliveries', 15));
    sampleData.add(new RadioModel(true, '30 Deliveries', 30));
    sampleData.add(new RadioModel(false, '60 Deliveries', 60));
    sampleData.add(new RadioModel(false, '90 Deliveries', 90));
    sampleDataDays.add(new RadioModelDays(true, 'Daily'));
    sampleDataDays.add(new RadioModelDays(false, 'Weekdays'));
    sampleDataDays.add(new RadioModelDays(false, 'Weekends'));
    sampleDataDays.add(new RadioModelDays(false, 'Custom'));
    sampleDataDaysCustom.add(new RadioModelDaysCustom(true, 'M'));
    sampleDataDaysCustom.add(new RadioModelDaysCustom(true, 'TU'));
    sampleDataDaysCustom.add(new RadioModelDaysCustom(true, 'W'));
    sampleDataDaysCustom.add(new RadioModelDaysCustom(true, 'TH'));
    sampleDataDaysCustom.add(new RadioModelDaysCustom(true, 'F'));
    sampleDataDaysCustom.add(new RadioModelDaysCustom(true, 'SA'));
    sampleDataDaysCustom.add(new RadioModelDaysCustom(true, 'SU'));
  }

  _showModalBottomSheet(context) {
    showModalBottomSheet(
      context: context,
      builder: (BuildContext context) {
        return StatefulBuilder(
          builder: (BuildContext context, setState) {
            return Container(
              height: 500,
              decoration: BoxDecoration(
                boxShadow: <BoxShadow>[
                  BoxShadow(
                      color: HexColor(widget.mealsListData.endColor)
                          .withOpacity(0.6),
                      offset: const Offset(1.1, 4.0),
                      blurRadius: 8.0),
                ],
                gradient: LinearGradient(
                  colors: <HexColor>[
                    HexColor(widget.mealsListData.startColor),
                    HexColor(widget.mealsListData.endColor),
                  ],
                  begin: Alignment.topLeft,
                  end: Alignment.bottomRight,
                ),
                borderRadius: const BorderRadius.only(
                  bottomRight: Radius.circular(8.0),
                  bottomLeft: Radius.circular(8.0),
                  topLeft: Radius.circular(8.0),
                  topRight: Radius.circular(54.0),
                ),
              ),
              child: Padding(
                padding: const EdgeInsets.only(
                    top: 16, left: 16, right: 16, bottom: 8),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Stack(
                          children: <Widget>[
                            Container(
                              width: 100,
                              height: 100,
                              decoration: BoxDecoration(
                                color: DashboardAppTheme.nearlyWhite
                                    .withOpacity(0.2),
                                shape: BoxShape.circle,
                              ),
                            ),
                            Positioned(
                              top: 0,
                              left: 0,
                              child: SizedBox(
                                width: 100,
                                height: 100,
                                child:
                                    Image.asset(widget.mealsListData.imagePath),
                              ),
                            )
                          ],
                        ),
                        SizedBox(
                          width: 20,
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              widget.mealsListData.titleTxt,
                              textAlign: TextAlign.center,
                              style: kItemBoxStyle,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: <Widget>[
                                Text(
                                  widget.mealsListData.kacl.toString(),
                                  textAlign: TextAlign.center,
                                  style: kItemBoxStyle,
                                ),
                                Padding(
                                  padding:
                                      const EdgeInsets.only(left: 4, bottom: 3),
                                  child: Text(
                                    'kcal',
                                    style: TextStyle(
                                      fontFamily: DashboardAppTheme.fontName,
                                      fontWeight: FontWeight.w500,
                                      fontSize: 14,
                                      letterSpacing: 0.2,
                                      color: DashboardAppTheme.white,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Text('₹ ${widget.mealsListData.price.toString()}',
                                textAlign: TextAlign.center,
                                style: kItemBoxStyle),
                          ],
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    SizedBox(
                      width: double.infinity,
                      height: 2.0,
                      child: Container(
                        color: Colors.white,
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    modalSteps(),
                  ],
                ),
              ),
            );
          },
        );
      },
    );
  }

  Widget modalSteps() {
    return StatefulBuilder(
      builder: (BuildContext context, setState) {
        return showOne
            ? Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text('How many units do you want?',
                      textAlign: TextAlign.left, style: kItemBoxStyle),
                  SizedBox(
                    height: 20,
                  ),
                  Row(
                    children: <Widget>[
                      Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            '${pkts.toString()} Pkt',
                            textAlign: TextAlign.left,
                            style: kItemBoxStyle,
                          ),
                          Text(
                            'per delivery',
                            textAlign: TextAlign.left,
                            style: kItemBoxStyleNoBold,
                          ),
                        ],
                      ),
                      Spacer(),
                      IconButton(
                        color: Colors.white,
                        onPressed: () {
                          setState(() {
                            if (pkts > 1) {
                              pkts -= 1;
                            }
                          });
                        },
                        icon: Icon(Icons.remove_circle_outline),
                      ),
                      Text(
                        pkts.toString(),
                        textAlign: TextAlign.left,
                        style: kItemBoxStyle,
                      ),
                      IconButton(
                        color: Colors.white,
                        onPressed: () {
                          setState(() {
                            pkts += 1;
                          });
                        },
                        icon: Icon(Icons.add_circle_outline),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 50,
                  ),
                  Text(
                    'Don\'t want to subscribe?',
                    style: kItemBoxStyleNoBold,
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  InkWell(
                    onTap: () {
                      print('deliver once');
                    },
                    child: Text(
                      'DELIVER ONCE',
                      style: kItemBoxStyleBlack,
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  FlatButton(
                    onPressed: () {
                      setState(() {
                        showOne = false;
                        showTwo = true;
                      });
                    },
                    child: SizedBox(
                      width: double.infinity,
                      height: 40.0,
                      child: Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(
                            Radius.circular(5),
                          ),
                          color: DashboardAppTheme.nearlyWhite,
                        ),
                        child: Center(
                          child: Text(
                            'Next',
                            style: TextStyle(
                              color: DashboardAppTheme.darkerText,
                              fontSize: 20,
                              fontFamily: 'CM Sans Serif',
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              )
            : showTwo
                ? Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          Icon(
                            Icons.arrow_back_ios,
                            color: DashboardAppTheme.nearlyWhite,
                            size: 20.0,
                          ),
                          InkWell(
                            onTap: () {
                              setState(() {
                                showOne = true;
                                showTwo = false;
                              });
                            },
                            child: Text(
                              'Back',
                              style: kItemBoxStyleNoBold,
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      SizedBox(
                        height: 182,
                        child: GridView.builder(
                          scrollDirection: Axis.vertical,
                          gridDelegate:
                              SliverGridDelegateWithFixedCrossAxisCount(
                                  crossAxisCount: 2, childAspectRatio: 3),
                          itemCount: sampleData.length,
                          itemBuilder: (BuildContext context, int index) {
                            return new InkWell(
                              //highlightColor: Colors.red,
                              onTap: () {
                                setState(() {
                                  sampleData.forEach(
                                      (element) => element.isSelected = false);
                                  sampleData[index].isSelected = true;
                                  count = sampleData[index].count;
                                  print(count);
                                });
                              },
                              child: new RadioItem(sampleData[index]),
                            );
                          },
                        ),
                      ),
                      FlatButton(
                        onPressed: () {
                          setState(() {
                            showOne = false;
                            showTwo = false;
                            showThree = true;
                          });
                        },
                        child: SizedBox(
                          width: double.infinity,
                          height: 40.0,
                          child: Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.all(
                                Radius.circular(5),
                              ),
                              color: DashboardAppTheme.nearlyWhite,
                            ),
                            child: Center(
                              child: Text(
                                'Next',
                                style: TextStyle(
                                  color: DashboardAppTheme.darkerText,
                                  fontSize: 20,
                                  fontFamily: 'CM Sans Serif',
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  )
                : showThree
                    ? Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              Icon(
                                Icons.arrow_back_ios,
                                color: DashboardAppTheme.nearlyWhite,
                                size: 20.0,
                              ),
                              InkWell(
                                onTap: () {
                                  setState(() {
                                    showOne = false;
                                    showTwo = true;
                                    showThree = false;
                                  });
                                },
                                child: Text(
                                  'Back',
                                  style: kItemBoxStyleNoBold,
                                ),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          SizedBox(
                            height: 122,
                            child: GridView.builder(
                              scrollDirection: Axis.vertical,
                              gridDelegate:
                                  SliverGridDelegateWithFixedCrossAxisCount(
                                      crossAxisCount: 2, childAspectRatio: 3),
                              itemCount: sampleDataDays.length,
                              itemBuilder: (BuildContext context, int index) {
                                return new InkWell(
                                  onTap: () {
                                    setState(() {
                                      sampleDataDays.forEach((element) =>
                                          element.isSelected = false);
                                      sampleDataDays[index].isSelected = true;
                                      days = sampleDataDays[index].buttonText;
                                      print(days);
                                      if (days == 'Daily') {
                                        sampleDataDaysCustom.forEach((element) {
                                          element.isSelected = true;
                                        });
                                        customDays = [
                                          'M',
                                          'TU',
                                          'W',
                                          'TH',
                                          'F',
                                          'SA',
                                          'SU'
                                        ];
                                        print(customDays);
                                      } else if (days == 'Weekdays') {
                                        for (int i = 5;
                                            i < sampleDataDaysCustom.length;
                                            i++) {
                                          sampleDataDaysCustom[i].isSelected =
                                              false;
                                        }
                                        for (int i = 0;
                                            i < sampleDataDaysCustom.length - 2;
                                            i++) {
                                          sampleDataDaysCustom[i].isSelected =
                                              true;
                                        }
                                        customDays = [
                                          'M',
                                          'TU',
                                          'W',
                                          'TH',
                                          'F',
                                        ];
                                        print(customDays);
                                      } else if (days == 'Weekends') {
                                        for (int i = 5;
                                            i < sampleDataDaysCustom.length;
                                            i++) {
                                          sampleDataDaysCustom[i].isSelected =
                                              true;
                                        }
                                        for (int i = 0;
                                            i < sampleDataDaysCustom.length - 2;
                                            i++) {
                                          sampleDataDaysCustom[i].isSelected =
                                              false;
                                        }
                                        customDays = [
                                          'SA',
                                          'SU',
                                        ];
                                        print(customDays);
                                      }
                                    });
                                  },
                                  child: new RadioItem(sampleDataDays[index]),
                                );
                              },
                            ),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Container(
                            height: 40,
                            child: ListView.builder(
                              scrollDirection: Axis.horizontal,
                              itemCount: sampleDataDaysCustom.length,
                              itemBuilder: (BuildContext context, int index) {
                                return IgnorePointer(
                                  ignoring: days != 'Custom',
                                  child: new InkWell(
                                    onTap: () {
                                      setState(() {
                                        sampleDataDaysCustom[index].isSelected =
                                            !sampleDataDaysCustom[index]
                                                .isSelected;
                                        if (customDays.contains(
                                            sampleDataDaysCustom[index]
                                                .buttonText)) {
                                          customDays.remove(
                                              sampleDataDaysCustom[index]
                                                  .buttonText);
                                        } else {
                                          customDays.add(
                                              sampleDataDaysCustom[index]
                                                  .buttonText);
                                        }
                                        print(customDays);
                                      });
                                    },
                                    child: RadioItemCustom(
                                        sampleDataDaysCustom[index]),
                                  ),
                                );
                              },
                            ),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          FlatButton(
                            onPressed: () {
                              if (customDays.length != 0) {
                                setState(() {
                                  showOne = false;
                                  showTwo = false;
                                  showThree = false;
                                  showFour = true;
                                });
                              }
                            },
                            child: SizedBox(
                              width: double.infinity,
                              height: 40.0,
                              child: Container(
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.all(
                                    Radius.circular(5),
                                  ),
                                  color: DashboardAppTheme.nearlyWhite,
                                ),
                                child: Center(
                                  child: Text(
                                    'Next',
                                    style: TextStyle(
                                      color: DashboardAppTheme.darkerText,
                                      fontSize: 20,
                                      fontFamily: 'CM Sans Serif',
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      )
                    : showFour
                        ? Column(
                            children: <Widget>[
                              Row(
                                children: <Widget>[
                                  Icon(
                                    Icons.arrow_back_ios,
                                    color: DashboardAppTheme.nearlyWhite,
                                    size: 20.0,
                                  ),
                                  InkWell(
                                    onTap: () {
                                      setState(() {
                                        showOne = false;
                                        showTwo = false;
                                        showThree = true;
                                        showFour = false;
                                      });
                                    },
                                    child: Text(
                                      'Back',
                                      style: kItemBoxStyleNoBold,
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(
                                height: 10,
                              ),
                            ],
                          )
                        : SizedBox();
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: widget.animationController,
      builder: (BuildContext context, Widget child) {
        return FadeTransition(
          opacity: widget.animation,
          child: Transform(
            transform: Matrix4.translationValues(
                100 * (1.0 - widget.animation.value), 0.0, 0.0),
            child: SizedBox(
              width: 130,
              child: Stack(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(
                        top: 32, left: 8, right: 8, bottom: 16),
                    child: Container(
                      decoration: BoxDecoration(
                        boxShadow: <BoxShadow>[
                          BoxShadow(
                              color: HexColor(widget.mealsListData.endColor)
                                  .withOpacity(0.6),
                              offset: const Offset(1.1, 4.0),
                              blurRadius: 8.0),
                        ],
                        gradient: LinearGradient(
                          colors: <HexColor>[
                            HexColor(widget.mealsListData.startColor),
                            HexColor(widget.mealsListData.endColor),
                          ],
                          begin: Alignment.topLeft,
                          end: Alignment.bottomRight,
                        ),
                        borderRadius: const BorderRadius.only(
                          bottomRight: Radius.circular(8.0),
                          bottomLeft: Radius.circular(8.0),
                          topLeft: Radius.circular(8.0),
                          topRight: Radius.circular(54.0),
                        ),
                      ),
                      child: Padding(
                        padding: const EdgeInsets.only(
                            top: 54, left: 16, right: 16, bottom: 8),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              widget.mealsListData.titleTxt,
                              textAlign: TextAlign.center,
                              style: kItemBoxStyle,
                            ),
                            Expanded(
                              child: Padding(
                                padding:
                                    const EdgeInsets.only(top: 8, bottom: 8),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Text(
                                      widget.mealsListData.titleTxt,
                                      style: TextStyle(
                                        fontFamily: DashboardAppTheme.fontName,
                                        fontWeight: FontWeight.w500,
                                        fontSize: 10,
                                        letterSpacing: 0.2,
                                        color: DashboardAppTheme.white,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            InkWell(
                              onTap: () {
                                _showModalBottomSheet(context);
                              },
                              child: Container(
                                decoration: BoxDecoration(
                                  color: DashboardAppTheme.nearlyWhite,
                                  shape: BoxShape.circle,
                                  boxShadow: <BoxShadow>[
                                    BoxShadow(
                                        color: DashboardAppTheme.nearlyBlack
                                            .withOpacity(0.4),
                                        offset: Offset(8.0, 8.0),
                                        blurRadius: 8.0),
                                  ],
                                ),
                                child: Padding(
                                  padding: const EdgeInsets.all(6.0),
                                  child: Icon(
                                    Icons.add,
                                    color:
                                        HexColor(widget.mealsListData.endColor),
                                    size: 24,
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    top: 0,
                    left: 0,
                    child: Container(
                      width: 84,
                      height: 84,
                      decoration: BoxDecoration(
                        color: DashboardAppTheme.nearlyWhite.withOpacity(0.2),
                        shape: BoxShape.circle,
                      ),
                    ),
                  ),
                  Positioned(
                    top: 0,
                    left: 8,
                    child: SizedBox(
                      width: 80,
                      height: 80,
                      child: Image.asset(widget.mealsListData.imagePath),
                    ),
                  )
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}

class RadioItem extends StatelessWidget {
  final _item;
  RadioItem(this._item);
  @override
  Widget build(BuildContext context) {
    return new Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        new Container(
          height: 40.0,
          width: MediaQuery.of(context).size.width / 2.5,
          child: new Center(
            child: new Text(
              _item.buttonText,
              style: new TextStyle(
                  color: _item.isSelected
                      ? DashboardAppTheme.darkerText
                      : DashboardAppTheme.nearlyWhite,
                  fontFamily: 'CM Sans Serif',
                  fontSize: 18.0),
            ),
          ),
          decoration: new BoxDecoration(
            color: _item.isSelected
                ? DashboardAppTheme.nearlyWhite
                : Colors.transparent,
            border: new Border.all(
                width: 1.0, color: DashboardAppTheme.nearlyWhite),
            borderRadius: const BorderRadius.all(const Radius.circular(2.0)),
          ),
        ),
      ],
    );
  }
}

class RadioItemCustom extends StatelessWidget {
  final RadioModelDaysCustom _item;
  RadioItemCustom(this._item);
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(right: 11),
      child: new Container(
        height: 40.0,
        width: MediaQuery.of(context).size.width / 10,
        child: new Center(
          child: new Text(
            _item.buttonText,
            style: new TextStyle(
                color: _item.isSelected
                    ? DashboardAppTheme.darkerText
                    : DashboardAppTheme.nearlyWhite,
                fontFamily: 'CM Sans Serif',
                fontSize: 18.0),
          ),
        ),
        decoration: new BoxDecoration(
          color: _item.isSelected
              ? DashboardAppTheme.nearlyWhite
              : Colors.transparent,
          border:
              new Border.all(width: 1.0, color: DashboardAppTheme.nearlyWhite),
          borderRadius: const BorderRadius.all(const Radius.circular(2.0)),
        ),
      ),
    );
  }
}

class RadioModel {
  bool isSelected;
  final String buttonText;
  final int count;

  RadioModel(this.isSelected, this.buttonText, this.count);
}

class RadioModelDays {
  bool isSelected;
  final String buttonText;

  RadioModelDays(this.isSelected, this.buttonText);
}

class RadioModelDaysCustom {
  bool isSelected;
  final String buttonText;

  RadioModelDaysCustom(this.isSelected, this.buttonText);
}
