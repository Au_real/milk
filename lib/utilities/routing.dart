import 'package:flutter/material.dart';
import 'package:milk/screens/dashboard/dashboard.dart';
import 'package:milk/screens/forgot.dart';
import 'package:milk/screens/login.dart';
import 'package:milk/screens/otp.dart';
import 'package:milk/screens/signup.dart';

import '../screens/onboarding_screen.dart';

class RouteGenerator {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    // Getting arguments passed in while calling Navigator.pushNamed
    final args = settings.arguments;

    switch (settings.name) {
      case '/':
        return MaterialPageRoute(builder: (_) => OnboardingScreen());
      case '/login':
        return MaterialPageRoute(
          builder: (_) => Login(),
        );
      case '/signup':
        return MaterialPageRoute(
          builder: (_) => SignUp(),
        );
      case '/forgot':
        return MaterialPageRoute(
          builder: (_) => Forgot(),
        );
      case '/otp':
        return MaterialPageRoute(
          builder: (_) => Otp(),
        );
      case '/dashboard':
        return MaterialPageRoute(
          builder: (_) => DashboardHomeScreen(),
        );
      // If args is not of the correct type, return an error page.
      // You can also throw an exception while in development.
      // return _errorRoute();
      default:
        // If there is no such named route in the switch statement, e.g. /third
        return _errorRoute();
    }
  }

  static Route<dynamic> _errorRoute() {
    return MaterialPageRoute(builder: (_) {
      return Scaffold(
        appBar: AppBar(
          title: Text('Error'),
        ),
        body: Center(
          child: Text('ERROR'),
        ),
      );
    });
  }
}
