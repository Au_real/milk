import 'package:flutter/material.dart';

final kTitleStyle = TextStyle(
  color: Colors.white,
  fontFamily: 'CM Sans Serif',
  fontSize: 26.0,
  height: 1.5,
);

final kHeadingStyle = TextStyle(
  color: Colors.white,
  fontFamily: 'CM Sans Serif',
  fontSize: 40,
  fontWeight: FontWeight.bold,
);

final kTextStyle = TextStyle(
  color: Colors.white,
  fontFamily: 'CM Sans Serif',
  fontSize: 16,
);

final kTextStyleBlack = kTextStyle.copyWith(
  color: Colors.black,
);

final kTitleStyleBlack = kTitleStyle.copyWith(
  color: Colors.black,
);

final kSubtitleStyle = TextStyle(
  color: Colors.white,
  fontFamily: 'CM Sans Serif',
  fontSize: 18.0,
  height: 1.2,
);

final kSubtitleStyleBlack = kSubtitleStyle.copyWith(
  color: Colors.black,
);
